import sys, getopt
sys.path.append('./Domain')

from orchestrator import Orchestrator

try:
    opts, args = getopt.getopt(sys.argv[1:],"i:")
except getopt.GetoptError:
    print("camelotHeraldTool.py -p <importPrefix>")
    sys.exit(2) 

print("camelotHeraldTool")

prefix = ''

for opt, arg in opts: 
    if opt in ("-i", "--ifile"):
        prefix = arg

orchestrator = Orchestrator()

orchestrator.Initialize()

orchestrator.Run(prefix)