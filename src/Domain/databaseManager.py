import sqlite3

class DatabaseManager:
	def Initialize(self):
		print("DatabaseManager::Initialize")
		
		self.dbConnection = sqlite3.connect("camelotHerald.db")
		
		dbCursor = self.dbConnection.cursor()
		sqlCharacterTableCreation = 'CREATE TABLE IF NOT EXISTS Character('\
            'CharacterWebId NVARCHAR(10) NOT NULL PRIMARY KEY,'\
            '    Name NVARCHAR(64) NOT NULL,'\
            '    ServerName NVARCHAR(12) NOT NULL,'\
			'    ClassName NVARCHAR(24) NOT NULL,'\
            '    LastOnRangeId INTEGER,'\
			'    TotalKills INTEGER,'\
			'    TotalDeaths INTEGER,'\
			'    TotalDeathblows INTEGER,'\
            '    TotalSoloKills INTEGER,'\
            '    LastCharacterInfoUpdate DATETIME )'

		
		dbCursor.execute(sqlCharacterTableCreation)
		
	def ImportCharacter(self, character):
		dbCursor = self.dbConnection.cursor()
		sqlImportCharacter= 'REPLACE INTO Character('\
            'CharacterWebId, Name, ServerName, ClassName, '\
			' TotalKills, TotalDeaths, TotalDeathblows, TotalSoloKills )'\
            '    VALUES( "'+character.id+'","'+character.name+'","'+character.serverName+'","'+character.className+'",'+str(character.totalKills)\
			+ ','+str(character.totalDeaths)+','+str(character.totalDeathblows)+','+str(character.totalSoloKills)+')'

		
		dbCursor.execute(sqlImportCharacter)
		self.dbConnection.commit()