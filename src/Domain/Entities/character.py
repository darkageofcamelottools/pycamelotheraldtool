class Character:
	def __init__(self, id, name, serverName, className):
		self.id = id
		self.name = name
		self.serverName = serverName
		self.className = className
		
	def SetTotalKills(self, kills):
		self.totalKills = kills
		
	def SetTotalDeaths(self, deaths):
		self.totalDeaths = deaths
	
	def SetTotalDeathblows(self, deathblows):
		self.totalDeathblows = deathblows
		
	def SetTotalSoloKills(self, soloKills):
		self.totalSoloKills = soloKills