from camelotHeraldManager import CamelotHeraldManager
from databaseManager import DatabaseManager
from ahk import AHK

class Orchestrator:

	def Initialize(self):
		print("Orchestrator::Initialize")
		
		ahk = AHK()
		
		win = ahk.find_window(title=b'Chrome')

		ahk.show_tooltip("Orchestrator::Initialize", second=5, x=10, y=10)
		
		self.camelotHeraldManager = CamelotHeraldManager()
		
		self.databaseManager = DatabaseManager()
		
		self.camelotHeraldManager.Initialize(self.databaseManager);
		
		self.databaseManager.Initialize();
		
		
	  
	def Run(self, prefix):
		print("Orchestrator::Run")
		
		self.camelotHeraldManager.ImportCharacters(prefix)