import urllib.request, json

import sys
sys.path.append('./Domain/Entities')

from character import Character

class CamelotHeraldManager:
	def Initialize(self, databaseManager):
		print("CamelotHeraldManager::Initialize")
		
		self.charList = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
		
		self.databaseManager = databaseManager
		
	def ImportCharacters(self, prefix):
		print("CamelotHeraldManager::ImportCharacters")
		
		for f in self.charList:
			self.ImportCharactersWithApi(prefix,f)
			
	def ImportCharactersWithApi(self, prefix, char):
		print("CamelotHeraldManager::ImportCharactersWithApi : "+prefix+char)
		
		url = urllib.request.urlopen("http://api.camelotherald.com/character/search?name="+prefix+char+"&cluster=Ywain")
		data = json.loads(url.read().decode())
		
		size = len(data["results"])
		
		if size == 150:
			for f in self.charList:
				self.ImportCharactersWithApi(prefix+char,f)
		else:
			for f in data["results"]:
				url = urllib.request.urlopen("http://api.camelotherald.com/character/info/"+f["character_web_id"])
				characterData = json.loads(url.read().decode())
				character = Character(f["character_web_id"], f["name"], f["server_name"], f["class_name"])
				
				character.SetTotalKills(characterData["realm_war_stats"]["current"]["player_kills"]["total"]["kills"]);
				character.SetTotalDeaths(characterData["realm_war_stats"]["current"]["player_kills"]["total"]["deaths"]);
				character.SetTotalDeathblows(characterData["realm_war_stats"]["current"]["player_kills"]["total"]["death_blows"]);
				character.SetTotalSoloKills(characterData["realm_war_stats"]["current"]["player_kills"]["total"]["solo_kills"]);
				
				self.databaseManager.ImportCharacter(character)